<?php

use Illuminate\Database\Seeder;
use App\Imports\mDivisionImport;

class Master006DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Excel::import(new mDivisionImport, public_path('/seeder_file/master_006_division.xlsx'));
    }
}
