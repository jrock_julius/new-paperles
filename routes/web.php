<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>['auth','role:superadmin|ppc']],function(){
    Route::resource('user', 'mUserController');
    Route::resource('md', 'mManufactureDrawingController');

    Route::get('md_wp/wip/',[
        'as' => 'md_wip.wip',
        'uses' => 'mManufactureDrawingController@work_in_progress' 
    ]);

    Route::get('md_rl/rel',[
        'as' => 'md_rel.rel',
        'uses' => 'mManufactureDrawingController@released' 
    ]);

    Route::resource('is', 'mInspectionSheetController');

    Route::get('is_wp/wip',[
        'as' => 'is.wip',
        'uses' => 'mInspectionSheetController@work_in_progress' 
    ]);

    Route::get('is_rl/rel',[
        'as' => 'is.rel',
        'uses' => 'mInspectionSheetController@released' 
    ]);

    Route::resource('division', 'mDivisionController');
    Route::resource('position', 'mPositionController');
});
