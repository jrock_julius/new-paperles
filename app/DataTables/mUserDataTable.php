<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class mUserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        // return datatables()
        //     ->eloquent($query)
        //     ->addColumn('action', 'muserdatatable.action');
        return datatables($query)
            ->addIndexColumn()

            ->editColumn('division.division_name', function($request){
                if($request->division)
                    return $request->division->division_name;      
                else
                    return '';               
            })

            ->addColumn('edit', function($request){
                return view ('datatable._edit',[
                    'model'    => $request  ,
                    'edit_url' => route('user.edit', $request->id)
                ]);                       
            })

            ->addColumn('delete', function($request){    
                return view ('datatable._delete',[
                    'model'    => $request,
                    'delete_url' => route('user.destroy', $request->id),
                    'confirm_message' => 'Yakin mau menghapus?' . $request->nip . '?'
                ]);                 
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\mUserDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->with('division','position')->newQuery();
        // $query = User::with('plant_id','division_id','position_id')->select('*');
        // return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('muserdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(2)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('edit')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::computed('delete')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::make('DT_RowIndex')->orderable(false)->title('No'),
            Column::make('nip'),
            Column::make('name'),
            Column::make('email'),
            Column::make('division.division_name')->orderable(false)->title('Div'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mUser_' . date('YmdHis');
    }
}
