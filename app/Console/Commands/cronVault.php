<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\mFileIteration_vault;
use App\mFileResource_vault;
use App\mFolder_vault;
use App\mRevision_vault;
use App\mFileMaster_vault;
use App\mManufactureDrawing;
use App\mInspectionSheet;
use App\mProject;
use Carbon\Carbon;
use DB;

class cronVault extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:vault';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        date_default_timezone_set('Asia/Jakarta');

        $date = Carbon::today()->toDateString();

        $fileIteration = mFileIteration_vault::whereDate('CheckinDate', '2020-11-27')->orderBy('ResourceId','DESC')->orderBy('FileIterationId','DESC')->get();

        $unik = $fileIteration->unique('ResourceId');

        if ($unik) {

            foreach ($unik as $a) {
                
                $get_id_file_master = mFileResource_vault::where('ResourceId',$a->ResourceId)->first();
                
                $get_folder_id = mFileMaster_vault::where('FileMasterId',$get_id_file_master->FileMasterId)->first();
                
                //check File tersebut sudah pernah disimpan dalam database system drawinglist online
                $md_drawing = mManufactureDrawing::where('file_master_id',$get_id_file_master->FileMasterId)->where('file_name',$a->FileName)->first();  

                $get_revision = mRevision_vault::where('MasterId',$get_id_file_master->FileMasterId)
                ->orderBy('RevisionId','DESC')->first();

                $vault_path = mFolder_vault::where('FolderId',$get_folder_id->FolderId)->first();

                $get_project = explode('/',$vault_path->VaultPath);

                if (count($get_project) > 3) {
                    if ($get_project[2] == '99. Project Trial') {
                    
                        // document_type
                        $doc_type = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',131)
                        ->first();
    
                        if (!empty($doc_type)) {
                            $doc_type_value = $doc_type->value;
                        }else {
                            $doc_type_value = null;
                        }
    
                        $revision = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',40)
                        ->first();
    
                        if (!empty($doc_type)) {
                            $revision_value = $revision->value;
                        }else {
                            $revision_value = null;
                        }
    
                        // part_number
                        $part_no = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',81)
                        ->first();
    
                        if (!empty($part_no)) {
                            $part_no_value = $part_no->value;
                        }else {
                            $part_no_value = null;
                        }
    
                        // master_part
                        $master_part = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',172)
                        ->first();
    
                        if (!empty($master_part)) {
                            $master_part_value = $master_part->value;
                        }else {
                            $master_part_value = null;
                        }
    
                        // project
                        $project = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',89)
                        ->first();
    
                        if (!empty($project)) {
                            $project_value = $project->value;
                        }else {
                            $project_value = null;
                        }
    
                         // material
                         $material = DB::connection('sqlsrv')
                         ->table('Property')
                         ->selectRaw('cast([Value] as varchar) as value')
                         ->where('EntityID',$a->FileIterationId)
                         ->where('PropertyDefID',80)
                         ->first();
     
                         if (!empty($material)) {
                             $material_value = $material->value;
                         }else {
                             $material_value = null;
                         }
    
                        // destination shop
                        $dest_shop = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',136)
                        ->first();
    
                        if (!empty($dest_shop)) {
                            $dest_shop_value = $dest_shop->value;
                        }else {
                            $dest_shop_value = null;
                        }
    
                        // title name
                        $title_name = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',71)
                        ->first();
    
                        if (!empty($title_name)) {
                            $title_name_value = $title_name->value;
                        }else {
                            $title_name_value = null;
                        }
    
                        // time estimation 1
                        $time_est_1 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',137)
                        ->first();
    
                        if (!empty($time_est_1)) {
                            $time_est_1_value = $time_est_1->value;
                            $time_est_1_value = str_replace(',','.',$time_est_1_value);
                            $time_est_1_value = floatval($time_est_1_value);
                        }else {
                            $time_est_1_value = null;
                        }
    
                        // time estimation 2
                        $time_est_2 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',138)
                        ->first();
    
                        if (!empty($time_est_2)) {
                            $time_est_2_value = $time_est_2->value;
                            $time_est_2_value = str_replace(',','.',$time_est_2_value);
                            $time_est_2_value = floatval($time_est_2_value);
                        }else {
                            $time_est_2_value = null;
                        }
    
                        // time estimation 3
                        $time_est_3 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',139)
                        ->first();
    
                        if (!empty($time_est_3)) {
                            $time_est_3_value = $time_est_3->value;
                            $time_est_3_value = str_replace(',','.',$time_est_3_value);
                            $time_est_3_value = floatval($time_est_3_value);
                        }else {
                            $time_est_3_value = null;
                        }
    
                        // time estimation 4
                        $time_est_4 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',140)
                        ->first();
    
                        if (!empty($time_est_4)) {
                            $time_est_4_value = $time_est_4->value;
                            $time_est_4_value = str_replace(',','.',$time_est_4_value);
                            $time_est_4_value = floatval($time_est_4_value);
                        }else {
                            $time_est_4_value = null;
                        }
    
                        // time estimation 5
                        $time_est_5 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',141)
                        ->first();
    
                        if (!empty($time_est_5)) {
                            $time_est_5_value = $time_est_5->value;
                            $time_est_5_value = str_replace(',','.',$time_est_5_value);
                            $time_est_5_value = floatval($time_est_5_value);
                        }else {
                            $time_est_5_value = null;
                        }
    
                        // time estimation 6
                        $time_est_6 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',142)
                        ->first();
    
                        if (!empty($time_est_6)) {
                            $time_est_6_value = $time_est_6->value;
                            $time_est_6_value = str_replace(',','.',$time_est_6_value);
                            $time_est_6_value = floatval($time_est_6_value);
                        }else {
                            $time_est_6_value = null;
                        }
    
                        // time estimation 7
                        $time_est_7 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',143)
                        ->first();
    
                        if (!empty($time_est_7)) {
                            $time_est_7_value = $time_est_7->value;
                            $time_est_7_value = str_replace(',','.',$time_est_7_value);
                            $time_est_7_value = floatval($time_est_7_value);
                        }else {
                            $time_est_7_value = null;
                        }
    
                        // time estimation 8
                        $time_est_8 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',144)
                        ->first();
    
                        if (!empty($time_est_8)) {
                            $time_est_8_value = $time_est_8->value;
                            $time_est_8_value = str_replace(',','.',$time_est_8_value);
                            $time_est_8_value = floatval($time_est_8_value);
                        }else {
                            $time_est_8_value = null;
                        }
    
                        // working sequence 2
                        $work_seq_2 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',105)
                        ->first();
    
                        if (!empty($work_seq_2)) {
                            $work_seq_2_value = $work_seq_2->value;
                        }else {
                            $work_seq_2_value = null;
                        }
    
                        // working sequence 3
                        $work_seq_3 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',106)
                        ->first();
    
                        if (!empty($work_seq_3)) {
                            $work_seq_3_value = $work_seq_3->value;
                        }else {
                            $work_seq_3_value = null;
                        }
    
                        // working sequence 4
                        $work_seq_4 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',107)
                        ->first();
    
                        if (!empty($work_seq_4)) {
                            $work_seq_4_value = $work_seq_4->value;
                        }else {
                            $work_seq_4_value = null;
                        }
    
                        // working sequence 5
                        $work_seq_5 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',108)
                        ->first();
    
                        if (!empty($work_seq_5)) {
                            $work_seq_5_value = $work_seq_5->value;
                        }else {
                            $work_seq_5_value = null;
                        }
    
                        // working sequence 6
                        $work_seq_6 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',109)
                        ->first();
    
                        if (!empty($work_seq_6)) {
                            $work_seq_6_value = $work_seq_6->value;
                        }else {
                            $work_seq_6_value = null;
                        }
    
                        // working sequence 7
                        $work_seq_7 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',110)
                        ->first();
    
                        if (!empty($work_seq_7)) {
                            $work_seq_7_value = $work_seq_7->value;
                        }else {
                            $work_seq_7_value = null;
                        }
    
                        // working sequence 8
                        $work_seq_8 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',111)
                        ->first();
    
                        if (!empty($work_seq_8)) {
                            $work_seq_8_value = $work_seq_8->value;
                        }else {
                            $work_seq_8_value = null;
                        }
    
                        // shop name 1
                        $shop_1 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',103)
                        ->first();
    
                        if (!empty($shop_1)) {
                            $shop_1_value = $shop_1->value;
                        }else {
                            $shop_1_value = null;
                        }
    
                        // shop name 2
                        $shop_2 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',112)
                        ->first();
    
                        if (!empty($shop_2)) {
                            $shop_2_value = $shop_2->value;
                        }else {
                            $shop_2_value = null;
                        }
    
                        // shop name 3
                        $shop_3 = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',113)
                        ->first();
    
                        if (!empty($shop_3)) {
                            $shop_3_value = $shop_3->value;
                        }else {
                            $shop_3_value = null;
                        }
    
                        // no doc is
                        $no_doc_is = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',277)
                        ->first();
    
                        if (!empty($no_doc_is)) {
                            $no_doc_is_value = $no_doc_is->value;
                        }else {
                            $no_doc_is_value = null;
                        }
    
                        // author / prepared by
                        $author = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',66)
                        ->first();
    
                        if (!empty($author)) {
                            $author_value = $author->value;
                        }else {
                            $author_value = null;
                        }
    
                        // state
                        $state = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',37)
                        ->first();
    
                        if (!empty($state)) {
                            $state_value = $state->value;
                        }else {
                            $state_value = null;
                        }
    
                        // version
                        $version = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',2)
                        ->first();
    
                        if (!empty($version)) {
                            $version_value = $version->value;
                        }else {
                            $version_value = null;
                        }
    
    
                        // quantity/car
                        $qty = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',114)
                        ->first();
    
                        if (!empty($qty)) {
                            $qty_value = $qty->value;
                        }else {
                            $qty_value = null;
                        }
    
                        // description
                        $desc = DB::connection('sqlsrv')
                        ->table('Property')
                        ->selectRaw('cast([Value] as varchar) as value')
                        ->where('EntityID',$a->FileIterationId)
                        ->where('PropertyDefID',79)
                        ->first();
    
                        if (!empty($desc)) {
                            $desc_value = $desc->value;
                        }else {
                            $desc_value = null;
                        }
    
                        // cek apakah sudah ada di db paperless
                        if ($md_drawing) {
                            $md_drawing->revision = $get_revision->RevisionLabel;
                            $md_drawing->state = $state_value; 
                            $md_drawing->update();
                        }else { //jika data blm ada didatabase paperless
                            //check MD / IS
                            $project = mProject::where('project_name',$project_value )->first();
                            
                            if ($project) {

                                if ($doc_type_value == 'MD' || $doc_type_value == 'md') {
                                    $md = mManufactureDrawing::create([
                                        'file_master_id' => $get_id_file_master->FileMasterId,
                                        'file_name' => $a->FileName,
                                        'title' => $title_name_value,
                                        'path' => $vault_path->VaultPath,
                                        'no_doc_is' => $no_doc_is_value,
                                        'destination_shop' => $dest_shop_value,
                                        'revision' => $get_revision->RevisionLabel,
                                        'state' => $state_value,
                                        'version' => $version_value,
                                        'material' => $material_value,
                                        'master_part' => $master_part_value,
                                        'part_number' => $part_no_value,
                                        'document_type' => $doc_type_value,
                                        'project' => $project_value,
                                        'qty' => $qty_value,
                                        'description' => $desc_value,
                                        'shop_1' => $shop_1_value,
                                        'shop_2' => $shop_2_value,
                                        'shop_3' => $shop_3_value,
                                        'ts_1' => $time_est_1_value,
                                        'ts_2' => $time_est_2_value,
                                        'ts_3' => $time_est_3_value,
                                        'ts_4' => $time_est_4_value,
                                        'ts_5' => $time_est_5_value,
                                        'ts_6' => $time_est_6_value,
                                        'ts_7' => $time_est_7_value,
                                        'ts_8' => $time_est_8_value,
                                        'ws_2' => $work_seq_2_value,
                                        'ws_3' => $work_seq_3_value,
                                        'ws_4' => $work_seq_4_value,
                                        'ws_5' => $work_seq_5_value,
                                        'ws_6' => $work_seq_6_value,
                                        'ws_7' => $work_seq_7_value,
                                        'ws_8' => $work_seq_8_value,
                                        'prepared_by' => $author_value,
                                    ]);
                                }elseif ($doc_type_value == 'IS') {
                                    $is = mInspectionSheet::create([
                                        'file_master_id' => $get_id_file_master->FileMasterId,
                                        'file_name' => $a->FileName,
                                        'path' => $vault_path->VaultPath,
                                        'revision' => $get_revision->RevisionLabel,
                                        'state' => $state_value,
                                        'version' => $version_value,
                                        'document_type' => $doc_type_value,
                                        'project' => $project_value,
                                    ]);
                                } 
                            }else {
                                $project_save = mProject::create([
                                       'project_name' => $project_value,
                                       'project_description' => $project_value, 
                                ]);

                                if ($doc_type_value == 'MD' || $doc_type_value == 'md') {
                                    $md = mManufactureDrawing::create([
                                        'file_master_id' => $get_id_file_master->FileMasterId,
                                        'file_name' => $a->FileName,
                                        'title' => $title_name_value,
                                        'path' => $vault_path->VaultPath,
                                        'no_doc_is' => $no_doc_is_value,
                                        'destination_shop' => $dest_shop_value,
                                        'revision' => $get_revision->RevisionLabel,
                                        'state' => $state_value,
                                        'version' => $version_value,
                                        'material' => $material_value,
                                        'master_part' => $master_part_value,
                                        'part_number' => $part_no_value,
                                        'document_type' => $doc_type_value,
                                        'project' => $project_value,
                                        'qty' => $qty_value,
                                        'description' => $desc_value,
                                        'shop_1' => $shop_1_value,
                                        'shop_2' => $shop_2_value,
                                        'shop_3' => $shop_3_value,
                                        'ts_1' => $time_est_1_value,
                                        'ts_2' => $time_est_2_value,
                                        'ts_3' => $time_est_3_value,
                                        'ts_4' => $time_est_4_value,
                                        'ts_5' => $time_est_5_value,
                                        'ts_6' => $time_est_6_value,
                                        'ts_7' => $time_est_7_value,
                                        'ts_8' => $time_est_8_value,
                                        'ws_2' => $work_seq_2_value,
                                        'ws_3' => $work_seq_3_value,
                                        'ws_4' => $work_seq_4_value,
                                        'ws_5' => $work_seq_5_value,
                                        'ws_6' => $work_seq_6_value,
                                        'ws_7' => $work_seq_7_value,
                                        'ws_8' => $work_seq_8_value,
                                        'prepared_by' => $author_value,
                                    ]);
                                }elseif ($doc_type_value == 'IS') {
                                    $is = mInspectionSheet::create([
                                        'file_master_id' => $get_id_file_master->FileMasterId,
                                        'file_name' => $a->FileName,
                                        'path' => $vault_path->VaultPath,
                                        'revision' => $get_revision->RevisionLabel,
                                        'state' => $state_value,
                                        'version' => $version_value,
                                        'document_type' => $doc_type_value,
                                        'project' => $project_value,
                                    ]);
                                }
                            }
                              
                        }
                       
    
                    }else {
                        // echo $a->FileName;
                    }
                }
                

                
            }
            
        }else {
            echo 'tidak ada';
        }
    }
}
