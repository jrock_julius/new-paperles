{!! Form::model($model, ['url' => $active_url, 'method' => 'get'] ) !!}
{!! Form::button('<i class="fas fa-check-circle" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-success')) !!}
{!! Form::close() !!}
