<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_divisions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('division_code',10)->unique();
            $table->integer('parent_division_id')->unsigned()->nullable();
            $table->string('division_name');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_division_id')->references('id')->on('m_divisions')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_divisions');
    }
}
