<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\mDivision;
use App\mPosition;
use App\Imports\UserImport;


class Master001UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $role = Role::where('name','superadmin')->first();  
        if (!$role) {
            $suRole = new Role();
            $suRole->name = "superadmin";
            $suRole->display_name = "super admin";
            $suRole->save();

            $division = mDivision::where('division_code', 'IT')->first();
            $position = mPosition::where('position_code', 'STAF')->first();
            $user = new User();
            $user->nip = "admin";
            $user->name = "admin";
            $user->email = "adminman4@inka.co.id";
            $user->division_id = $division->id;
            $user->position_id = $position->id;
            $user->password = bcrypt('password');
            $user->save();

            $user->attachRole($suRole);
        }
        $role = Role::where('name','warehouse')->first();  
        if (!$role) {
            $warehouseRole = new Role();
            $warehouseRole->name = "warehouse";
            $warehouseRole->display_name = "warehouse";
            $warehouseRole->save();
        }
        $role = Role::where('name','ppc')->first();  
        if (!$role) {
            $ppcRole = new Role();
            $ppcRole->name = "ppc";
            $ppcRole->display_name = "ppc";
            $ppcRole->save();
        }
        $role = Role::where('name','operator')->first();  
        if (!$role) {
            $operatorRole = new Role();
            $operatorRole->name = "operator";
            $operatorRole->display_name = "operator";
            $operatorRole->save();
        }
        $role = Role::where('name','monitoring')->first();  
        if (!$role) {
            $monitoringRole = new Role();
            $monitoringRole->name = "monitoring";
            $monitoringRole->display_name = "Monitoring";
            $monitoringRole->save();
        }
        $role = Role::where('name','teklog')->first();  
        if (!$role) {
            $teklogRole = new Role();
            $teklogRole->name = "teklog";
            $teklogRole->display_name = "Tek & Log";
            $teklogRole->save();
        }

        Excel::import(new UserImport, public_path('/seeder_file/master_001_user.xlsx'));
    }
}
