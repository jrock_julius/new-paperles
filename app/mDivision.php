<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class mDivision extends Model implements AuditableContract
{
    //
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'division_code',
        'parent_division_id',
        'division_name',
    ];

    public function division(){
        return $this->belongsTo('App\mDivision','parent_division_id','id');
    }

}
