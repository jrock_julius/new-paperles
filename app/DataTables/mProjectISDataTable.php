<?php

namespace App\DataTables;

use App\mProject;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class mProjectISDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()

        ->addColumn('release', function($request){  
                      
            return view ('datatable._rel',[
            'model'    => $request,
            'active_url' => route('is.wip', $request->id),
                // 'confirm_message' => 'Yakin ingin close project ' . $request->project_code . '?'
             ]);                            
        })

        ->addColumn('work_in_progress', function($request){  
                      
            return view ('datatable._wip',[
            'model'    => $request,
            'active_url' => route('is.rel', $request->id),
                // 'confirm_message' => 'Yakin ingin close project ' . $request->project_code . '?'
             ]);                            
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\mProjectI $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(mProject $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('mprojectmd-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('release')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::computed('work_in_progress')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::make('DT_RowIndex')->orderable(false)->title('No'),
            Column::make('project_name'),
            Column::make('project_description'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mProjectIS_' . date('YmdHis');
    }
}
