{!! Form::model($model, ['url' => $active_url, 'method' => 'get'] ) !!}
{!! Form::button('<i class="fas fa-cogs" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-info')) !!}
{!! Form::close() !!}