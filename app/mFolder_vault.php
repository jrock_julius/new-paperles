<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mFolder_vault extends Model
{
    //
    protected $connection = 'sqlsrv';
    protected $table = 'Folder';
    protected $primarykey = 'FolderID';
    protected $fillable =['FolderID',
                         'FolderName',
                         'VaultPath',
                         'ParentFolderId',
];
}
