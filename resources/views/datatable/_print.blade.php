{!! Form::model($model, ['url' => $print_url, 'method' => 'get', 'class' => 'form-inline'] ) !!}
{!! Form::button('<i class="fa fa-print" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-success', 'data-toggle' => 'tooltip', 'title' => 'Print')) !!}
{!! Form::close() !!}