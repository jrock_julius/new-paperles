<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mFileIteration_vault extends Model
{
    //
    protected $connection = 'sqlsrv';
    protected $table = 'FileIteration';
    protected $primarykey = 'FileIterationId';
    protected $fillable =['FileIterationId',
                         'FileName',
                         'ModDate',
                         'CheckinDate',
                         'LifeCycleStateName',
                         'CheckOutDate',
                         'ResourceId',
];
}
