<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMManufactureDrawingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_manufacture_drawings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_master_id');
            $table->string('file_name');
            $table->string('title')->nullable();
            $table->string('path');
            $table->string('no_doc_is')->nullable();
            $table->string('destination_shop')->nullable();
            $table->string('revision')->nullable();
            $table->string('state')->nullable();
            $table->integer('version')->nullable();
            $table->string('material')->nullable();
            $table->string('master_part')->nullable();
            $table->string('part_number')->nullable();
            $table->string('document_type')->nullable();
            $table->string('project')->nullable();
            $table->string('qty')->nullable();
            $table->string('description')->nullable();
            $table->string('shop_1')->nullable();
            $table->string('shop_2')->nullable();
            $table->string('shop_3')->nullable();
            $table->double('ts_1')->nullable();
            $table->double('ts_2')->nullable();
            $table->double('ts_3')->nullable();
            $table->double('ts_4')->nullable();
            $table->double('ts_5')->nullable();
            $table->double('ts_6')->nullable();
            $table->double('ts_7')->nullable();
            $table->double('ts_8')->nullable();
            $table->string('ws_2')->nullable();
            $table->string('ws_3')->nullable();
            $table->string('ws_4')->nullable();
            $table->string('ws_5')->nullable();
            $table->string('ws_6')->nullable();
            $table->string('ws_7')->nullable();
            $table->string('ws_8')->nullable();
            $table->string('prepared_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_manufacture_drawings');
    }
}
