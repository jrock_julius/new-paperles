<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMInspectionSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_inspection_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_master_id');
            $table->string('file_name');
            $table->string('path');
            $table->string('revision')->nullable();
            $table->string('state')->nullable();
            $table->integer('version')->nullable();
            $table->string('document_type');
            $table->string('project')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_inspection_sheets');
    }
}
