<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mRevision_vault extends Model
{
    //
    protected $connection = 'sqlsrv';
    protected $table = 'Revision';
    protected $primarykey = 'RevisionId';
    protected $fillable =['RevisionId',
                         'RevisionDefinitionId',
                         'MasterId',
                         'RevisionLabel',
                         'RevisionOrder',
                         'MaxIterationId',
                         'MaxConsumableIterationId',
];
}
