<?php

use Illuminate\Database\Seeder;
use App\Imports\mPositionImport;

class Master015PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Excel::import(new mPositionImport, public_path('/seeder_file/master_015_position.xlsx'));
    }
}
