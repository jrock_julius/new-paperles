<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\mDivisionDataTable;
use App\mDivision;
use Session;

class mDivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(mDivisionDataTable $dataTable)
    {
        //
        return $dataTable->render('master.division.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.division.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'division_code'    => 'required|unique:m_divisions,division_code',
            'division_name'    => 'required'
        ]);

        $division = mDivision::create([
            'division_code'    => $request->division_code,
            'division_name'    => $request->division_name
        ]);

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $division->division_name"
        ]);

        return redirect()->route('division.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $division = mDivision::find($id);
        return view('master.division.edit', compact('division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request , [
            'division_code'    => 'required|unique:m_divisions,division_code,'.$id,
            'division_name'    => 'required'
        ]);

        $division = mDivision::find($id);
        $division->division_code  = $request->division_code;
        $division->division_name  = $request->division_name;
        $division->save();

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $division->division_code"
        ]);

        return redirect()->route('division.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = mDivision::find($id);

        mDivision::destroy($id);

        Session::flash("flash_notification",[
            "level" => "success",
            "message" => "Berhasil menghapus $division->division_name"
        ]);

        return redirect()->route("division.index");
    }
}
