@extends('adminlte::page')

@section('title', 'Manufacturing 4.0')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-3 col-6">
        <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $projects }}</h3>

                    <p>Projects</p>
                </div>
                <div class="icon">
                    <i class="fas fa-chart-pie"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{ $products }}</h3>

                    <p>Products</p>
                </div>
                <div class="icon">
                    <i class="fas fa-dolly-flatbed"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $onduty }}</h3>

                    <p>Operators On Duty</p>
                </div>
                <div class="icon">
                    <i class="fas fa-people-carry"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{ $available }}</h3>

                    <p>Operators Available</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-line"></i>
                         S-Curve
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-1">
                            {!! Form::label('mrp', 'Project', ['class'=>'form-label']) !!}
                        </div>
                        <div class="col-md-5">
                            {!! Form::select('mrp', $list_mrp->pluck('label','id')->all(), null, [ 'class' => 'form-control', 'id' => 'mrp']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="chart">
                        <canvas id="mixedChart" style="min-height: 250px; height: 500px; max-height: 500px; max-width: 100%;"></canvas>
                        {{--
                        <hr>
                        <canvas id="lineChart" style="min-height: 250px; height: 500px; max-height: 500px; max-width: 100%;"></canvas>
                        --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-bar"></i>
                        Duration (plan x actual) in minutes
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-1">
                            {!! Form::label('product', 'Product', ['class'=>'form-label']) !!}
                        </div>
                        <div class="col-md-5">
                            {!! Form::select('product', [], null, [ 'class' => 'form-control', 'id' => 'product']) !!}
                        </div>
                    </div>
                    <hr>
                    <div class="chart">
                        <canvas id="barChart" style="min-height: 250px; height: 750px; max-height: 750px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <!-- ChartJS -->
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
    <script> console.log('Hi!'); </script>
    <script>
        $(document).ready(function() {
            var mrp_id = $('#mrp').val();
            selectionProduct(mrp_id);
            // getScurve(mrp_id);
            getMixedChart(mrp_id);
        });

        $('#mrp').on('change', function (e) {
            var mrp_id = this.value;
            selectionProduct(mrp_id);
            // getScurve(mrp_id);
            getMixedChart(mrp_id);
        });

        $('#product').on('change', function (e) {
            var product_id = this.value;
            getBar(product_id);
        });

        function selectionProduct(mrp_id){
            var link = "{{ url('/dashboard/product') }}" + "/" + mrp_id;
        
            $.get(link, function(data) {
                var select = $('#product').selectize(data);
                var selectize = select[0].selectize;
                selectize.clearOptions()

                var i = 0;
                $.each(data, function(key, value) {
                    if(i == 0) 
                        i = key;
                    
                    selectize.addOption({value: key, text: value});
                    // selectize.refreshOptions();
                });
                selectize.setValue(i);
            });
        }

        function getBar(product_id){
            var link = "{{ url('/dashboard/duration') }}"+'/'+product_id;
            var areaChartData;
            var barChartOptions;
            var barChartCanvas;
            var barChartData;
            var barChart;
	
            $.get(link, function(data) {
                areaChartData = {
                    labels  : data.process,
                    datasets: [
                        {
                            label               : 'Actual',
                            backgroundColor     : data.color,
                            borderColor         : data.color,
                            // borderColor         : 'rgba(60,141,188,0.8)',
                            pointRadius          : false,
                            pointColor          : '#3b8bba',
                            pointStrokeColor    : 'rgba(60,141,188,1)',
                            pointHighlightFill  : '#fff',
                            pointHighlightStroke: 'rgba(60,141,188,1)',
                            data                : data.actual
                        },
                        {
                            label               : 'Plan',
                            backgroundColor     : 'rgba(210, 214, 222, 1)',
                            borderColor         : 'rgba(210, 214, 222, 1)',
                            pointRadius         : false,
                            pointColor          : 'rgba(210, 214, 222, 1)',
                            pointStrokeColor    : '#c1c7d1',
                            pointHighlightFill  : '#fff',
                            pointHighlightStroke: 'rgba(220,220,220,1)',
                            data                : data.plan
                        },
                    ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                barChartCanvas = $('#barChart').get(0).getContext('2d');
                barChartData = jQuery.extend(true, {}, areaChartData);
                // var temp0 = areaChartData.datasets[0]
                // var temp1 = areaChartData.datasets[1]
                // barChartData.datasets[0] = temp1
                // barChartData.datasets[1] = temp0

                barChartOptions = {
                    responsive              : true,
                    maintainAspectRatio     : false,
                    datasetFill             : false
                }
                
                if(window.barChart instanceof HTMLCanvasElement)
                {
                    console.log("canvas");
                }
                else
                {
                    window.barChart.destroy();
                }
                window.barChart = new Chart(barChartCanvas, {
                // barChart = new Chart(barChartCanvas, {
                    type: 'horizontalBar', 
                    data: barChartData,
                    options: barChartOptions
                });
            });
        }

        function getMixedChart(mrp_id){
            var ctx = document.getElementById('mixedChart');
            var link = "{{ url('/dashboard/mixed') }}"+'/'+mrp_id;
            
            $.get(link, function(data) {
                var mixedChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        datasets: [{
                            label           : 'Actual Labour',
                            data            : data.current,
                            backgroundColor : 'rgba(60,141,188,0.9)',
                            // this dataset is drawn below
                            order           : 2
                        }, {
                            label           : 'Actual Labour (accumulate)',
                            data            : data.accumulate,
                            lineTension     : 0.1,
                            type            : 'line',
                            backgroundColor : 'rgba(204,0,0,1)',
                            borderColor     : 'rgba(204,0,0,1)',
                            fill            : false,
                            // this dataset is drawn on top
                            order           : 1
                        }],
                        labels: data.label_x
                    },
                    options: {
                        maintainAspectRatio : false,
                        responsive : true,
                        legend: {
                            display: true
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });
        }

        function getScurve(mrp_id){
            var areaChartData;
            var areaChartOptions;
            var lineChartCanvas;
            var lineChartOptions;
            var lineChartData;
            var lineChart;
            var dataset = [];
            var link = "{{ url('/dashboard/scurve') }}"+'/'+mrp_id;
	
            $.get(link, function(data) {                
                var i=0;
                var color;
                data.products.forEach(function (product) { 
                    // color = random_rgba();
                    color = materialColor();

                    dataset[i] = {
                        label               : product,
                        lineTension         : 0.1,
                        backgroundColor     : color,
                        borderColor         : color,
                        pointRadius         : 3,
                        pointHoverRadius    : 5,
                        pointColor          : '#3b8bba',
                        pointStrokeColor    : 'rgba(60,141,188,1)',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data                : data.datasets[product]
                    };
                    i++;
                }); 

                areaChartData = {
                    labels  : data.label_x,
                    datasets: dataset
                }

                areaChartOptions = {
                    maintainAspectRatio : false,
                    responsive : true,
                    legend: {
                        display: true
                    },
                    scales: {
                        xAxes: [{
                            gridLines : {
                                display : false,
                            },
                            ticks: {
                                autoSkip: false,
                                maxRotation: 90,
                                minRotation: 90
                            }
                        }],
                        yAxes: [{
                            gridLines : {
                                display : true,
                            }
                        }]
                    }
                }

                //-------------
                //- LINE CHART -
                //--------------
                lineChartCanvas = $('#lineChart').get(0).getContext('2d')
                lineChartOptions = jQuery.extend(true, {}, areaChartOptions)
                lineChartData = jQuery.extend(true, {}, areaChartData)
                var i=0;
                data.products.forEach(function (product) { 
                    lineChartData.datasets[i].fill = false;
                    i++;
                });
                lineChartOptions.datasetFill = false;

                lineChart = new Chart(lineChartCanvas, { 
                    type: 'line',
                    data: lineChartData, 
                    options: lineChartOptions
                });
            });
        }

        function random_rgba() {
            var o = Math.round, r = Math.random, s = 255;
            return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
        }

        function materialColor() {
            // colors from https://github.com/egoist/color-lib/blob/master/color.json
            var colors = {
                "red": {
                    "50": "#ffebee",
                    "100": "#ffcdd2",
                    "200": "#ef9a9a",
                    "300": "#e57373",
                    "400": "#ef5350",
                    "500": "#f44336",
                    "600": "#e53935",
                    "700": "#d32f2f",
                    "800": "#c62828",
                    "900": "#b71c1c",
                    "hex": "#f44336",
                    "a100": "#ff8a80",
                    "a200": "#ff5252",
                    "a400": "#ff1744",
                    "a700": "#d50000"
                },
                "pink": {
                    "50": "#fce4ec",
                    "100": "#f8bbd0",
                    "200": "#f48fb1",
                    "300": "#f06292",
                    "400": "#ec407a",
                    "500": "#e91e63",
                    "600": "#d81b60",
                    "700": "#c2185b",
                    "800": "#ad1457",
                    "900": "#880e4f",
                    "hex": "#e91e63",
                    "a100": "#ff80ab",
                    "a200": "#ff4081",
                    "a400": "#f50057",
                    "a700": "#c51162"
                },
                "purple": {
                    "50": "#f3e5f5",
                    "100": "#e1bee7",
                    "200": "#ce93d8",
                    "300": "#ba68c8",
                    "400": "#ab47bc",
                    "500": "#9c27b0",
                    "600": "#8e24aa",
                    "700": "#7b1fa2",
                    "800": "#6a1b9a",
                    "900": "#4a148c",
                    "hex": "#9c27b0",
                    "a100": "#ea80fc",
                    "a200": "#e040fb",
                    "a400": "#d500f9",
                    "a700": "#aa00ff"
                },
                "deepPurple": {
                    "50": "#ede7f6",
                    "100": "#d1c4e9",
                    "200": "#b39ddb",
                    "300": "#9575cd",
                    "400": "#7e57c2",
                    "500": "#673ab7",
                    "600": "#5e35b1",
                    "700": "#512da8",
                    "800": "#4527a0",
                    "900": "#311b92",
                    "hex": "#673ab7",
                    "a100": "#b388ff",
                    "a200": "#7c4dff",
                    "a400": "#651fff",
                    "a700": "#6200ea"
                },
                "indigo": {
                    "50": "#e8eaf6",
                    "100": "#c5cae9",
                    "200": "#9fa8da",
                    "300": "#7986cb",
                    "400": "#5c6bc0",
                    "500": "#3f51b5",
                    "600": "#3949ab",
                    "700": "#303f9f",
                    "800": "#283593",
                    "900": "#1a237e",
                    "hex": "#3f51b5",
                    "a100": "#8c9eff",
                    "a200": "#536dfe",
                    "a400": "#3d5afe",
                    "a700": "#304ffe"
                },
                "blue": {
                    "50": "#e3f2fd",
                    "100": "#bbdefb",
                    "200": "#90caf9",
                    "300": "#64b5f6",
                    "400": "#42a5f5",
                    "500": "#2196f3",
                    "600": "#1e88e5",
                    "700": "#1976d2",
                    "800": "#1565c0",
                    "900": "#0d47a1",
                    "hex": "#2196f3",
                    "a100": "#82b1ff",
                    "a200": "#448aff",
                    "a400": "#2979ff",
                    "a700": "#2962ff"
                },
                "lightBlue": {
                    "50": "#e1f5fe",
                    "100": "#b3e5fc",
                    "200": "#81d4fa",
                    "300": "#4fc3f7",
                    "400": "#29b6f6",
                    "500": "#03a9f4",
                    "600": "#039be5",
                    "700": "#0288d1",
                    "800": "#0277bd",
                    "900": "#01579b",
                    "hex": "#03a9f4",
                    "a100": "#80d8ff",
                    "a200": "#40c4ff",
                    "a400": "#00b0ff",
                    "a700": "#0091ea"
                },
                "cyan": {
                    "50": "#e0f7fa",
                    "100": "#b2ebf2",
                    "200": "#80deea",
                    "300": "#4dd0e1",
                    "400": "#26c6da",
                    "500": "#00bcd4",
                    "600": "#00acc1",
                    "700": "#0097a7",
                    "800": "#00838f",
                    "900": "#006064",
                    "hex": "#00bcd4",
                    "a100": "#84ffff",
                    "a200": "#18ffff",
                    "a400": "#00e5ff",
                    "a700": "#00b8d4"
                },
                "teal": {
                    "50": "#e0f2f1",
                    "100": "#b2dfdb",
                    "200": "#80cbc4",
                    "300": "#4db6ac",
                    "400": "#26a69a",
                    "500": "#009688",
                    "600": "#00897b",
                    "700": "#00796b",
                    "800": "#00695c",
                    "900": "#004d40",
                    "hex": "#009688",
                    "a100": "#a7ffeb",
                    "a200": "#64ffda",
                    "a400": "#1de9b6",
                    "a700": "#00bfa5"
                },
                "green": {
                    "50": "#e8f5e9",
                    "100": "#c8e6c9",
                    "200": "#a5d6a7",
                    "300": "#81c784",
                    "400": "#66bb6a",
                    "500": "#4caf50",
                    "600": "#43a047",
                    "700": "#388e3c",
                    "800": "#2e7d32",
                    "900": "#1b5e20",
                    "hex": "#4caf50",
                    "a100": "#b9f6ca",
                    "a200": "#69f0ae",
                    "a400": "#00e676",
                    "a700": "#00c853"
                },
                "lightGreen": {
                    "50": "#f1f8e9",
                    "100": "#dcedc8",
                    "200": "#c5e1a5",
                    "300": "#aed581",
                    "400": "#9ccc65",
                    "500": "#8bc34a",
                    "600": "#7cb342",
                    "700": "#689f38",
                    "800": "#558b2f",
                    "900": "#33691e",
                    "hex": "#8bc34a",
                    "a100": "#ccff90",
                    "a200": "#b2ff59",
                    "a400": "#76ff03",
                    "a700": "#64dd17"
                },
                "lime": {
                    "50": "#f9fbe7",
                    "100": "#f0f4c3",
                    "200": "#e6ee9c",
                    "300": "#dce775",
                    "400": "#d4e157",
                    "500": "#cddc39",
                    "600": "#c0ca33",
                    "700": "#afb42b",
                    "800": "#9e9d24",
                    "900": "#827717",
                    "hex": "#cddc39",
                    "a100": "#f4ff81",
                    "a200": "#eeff41",
                    "a400": "#c6ff00",
                    "a700": "#aeea00"
                },/*
                "yellow": {
                    "50": "#fffde7",
                    "100": "#fff9c4",
                    "200": "#fff59d",
                    "300": "#fff176",
                    "400": "#ffee58",
                    "500": "#ffeb3b",
                    "600": "#fdd835",
                    "700": "#fbc02d",
                    "800": "#f9a825",
                    "900": "#f57f17",
                    "hex": "#ffeb3b",
                    "a100": "#ffff8d",
                    "a200": "#ffff00",
                    "a400": "#ffea00",
                    "a700": "#ffd600"
                },*/
                "amber": {
                    "50": "#fff8e1",
                    "100": "#ffecb3",
                    "200": "#ffe082",
                    "300": "#ffd54f",
                    "400": "#ffca28",
                    "500": "#ffc107",
                    "600": "#ffb300",
                    "700": "#ffa000",
                    "800": "#ff8f00",
                    "900": "#ff6f00",
                    "hex": "#ffc107",
                    "a100": "#ffe57f",
                    "a200": "#ffd740",
                    "a400": "#ffc400",
                    "a700": "#ffab00"
                },
                "orange": {
                    "50": "#fff3e0",
                    "100": "#ffe0b2",
                    "200": "#ffcc80",
                    "300": "#ffb74d",
                    "400": "#ffa726",
                    "500": "#ff9800",
                    "600": "#fb8c00",
                    "700": "#f57c00",
                    "800": "#ef6c00",
                    "900": "#e65100",
                    "hex": "#ff9800",
                    "a100": "#ffd180",
                    "a200": "#ffab40",
                    "a400": "#ff9100",
                    "a700": "#ff6d00"
                },
                "deepOrange": {
                    "50": "#fbe9e7",
                    "100": "#ffccbc",
                    "200": "#ffab91",
                    "300": "#ff8a65",
                    "400": "#ff7043",
                    "500": "#ff5722",
                    "600": "#f4511e",
                    "700": "#e64a19",
                    "800": "#d84315",
                    "900": "#bf360c",
                    "hex": "#ff5722",
                    "a100": "#ff9e80",
                    "a200": "#ff6e40",
                    "a400": "#ff3d00",
                    "a700": "#dd2c00"
                },
                "brown": {
                    "50": "#efebe9",
                    "100": "#d7ccc8",
                    "200": "#bcaaa4",
                    "300": "#a1887f",
                    "400": "#8d6e63",
                    "500": "#795548",
                    "600": "#6d4c41",
                    "700": "#5d4037",
                    "800": "#4e342e",
                    "900": "#3e2723",
                    "hex": "#795548"
                },
                "grey": {
                    "50": "#fafafa",
                    "100": "#f5f5f5",
                    "200": "#eeeeee",
                    "300": "#e0e0e0",
                    "400": "#bdbdbd",
                    "500": "#9e9e9e",
                    "600": "#757575",
                    "700": "#616161",
                    "800": "#424242",
                    "900": "#212121",
                    "hex": "#9e9e9e"
                },
                "blueGrey": {
                    "50": "#eceff1",
                    "100": "#cfd8dc",
                    "200": "#b0bec5",
                    "300": "#90a4ae",
                    "400": "#78909c",
                    "500": "#607d8b",
                    "600": "#546e7a",
                    "700": "#455a64",
                    "800": "#37474f",
                    "900": "#263238",
                    "hex": "#607d8b"
                }/*,
                "black": {
                    "hex": "#000000"
                },
                "white": {
                    "hex": "#ffffff"
                }*/
                }
                // pick random property
                //var property = pickRandomProperty(colors);
            var colorList = colors[pickRandomProperty(colors)];
            var newColorKey = pickRandomProperty(colorList);
            var newColor = colorList[newColorKey];
            
            return newColor;
        }

        function pickRandomProperty(obj) {
            var result;
            var count = 0;
            for (var prop in obj) {
                if (Math.random() < 1 / ++count) {
                    result = prop;
                }
            }
            
            return result;
        }
    </script>
@stop