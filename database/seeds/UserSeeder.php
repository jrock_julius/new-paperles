<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Super
        $role = Role::where('name','superadmin')->first();  
        if (!$role) {
            $suRole = new Role();
            $suRole->name = "superadmin";
            $suRole->display_name = "super admin";
            $suRole->save();

            $user = new User();
            $user->nip = "admin";
            $user->name = "admin";
            $user->email = "setiady.pjati@inka.co.id";
            $user->password = bcrypt('password');
            $user->save();

            $user->attachRole($suRole);
        }
        $role = Role::where('name','ppc')->first();
        if (!$role) {
            $ppcRole = new Role();
            $ppcRole->name = "ppc";
            $ppcRole->display_name = "PPC";
            $ppcRole->save();

            $ppc = new User();
            $ppc->nip = "ppc";
            $ppc->name = "PPC";
            $ppc->email = "ppc@inka.co.id";
            $ppc->password = bcrypt('password');
            $ppc->save();

            $ppc->attachRole($ppcRole);
        }
        $role = Role::where('name','drafter')->first();
        if (!$role) {
            $drafterRole = new Role();
            $drafterRole->name = "drafter";
            $drafterRole->display_name = "drafter";
            $drafterRole->save();

            $drafter = new User();
            $drafter->nip = "drafter";
            $drafter->name = "drafter";
            $drafter->email = "drafter@inka.co.id";
            $drafter->password = bcrypt('password');
            $drafter->save();

            $drafter->attachRole($drafterRole);
        }
        $role = Role::where('name','eim')->first();
        if (!$role) {
            $eimRole = new Role();
            $eimRole->name = "eim";
            $eimRole->display_name = "eim";
            $eimRole->save();

            $eim = new User();
            $eim->nip = "eim";
            $eim->name = "eim";
            $eim->email = "eim@inka.co.id";
            $eim->password = bcrypt('password');
            $eim->save();

            $eim->attachRole($eimRole);
        }
        $role = Role::where('name','qc')->first();
        if (!$role) {
            $qcRole = new Role();
            $qcRole->name = "qc";
            $qcRole->display_name = "qc";
            $qcRole->save();

            $qc = new User();
            $qc->nip = "qc";
            $qc->name = "qc";
            $qc->email = "qc@inka.co.id";
            $qc->password = bcrypt('password');
            $qc->save();

            $qc->attachRole($qcRole);
        }
    }
}
