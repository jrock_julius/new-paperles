<div class="row p-t-20">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('position_code') ? 'has-error': '' }} ">
            {!! Form::label('position_code', 'Position Code', ['class'=>'form-label']) !!}
            {!! Form::text('position_code', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('position_code', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('position_name') ? 'has-error': '' }} ">
            {!! Form::label('position_name', 'Position Name', ['class'=>'form-label']) !!}
            {!! Form::text('position_name', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('position_name', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
</div>

{!! Form::submit ('Simpan',['class'=>'btn btn-primary m-b-10 m-l-5']) !!}