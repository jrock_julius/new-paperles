<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nip',10)->unique();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('division_id')->unsigned()->nullable();
            $table->integer('position_id')->unsigned()->nullable();
            $table->string('email');
            // $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('id_card')->nullable();
            $table->string('path')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('m_divisions')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('position_id')->references('id')->on('m_positions')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
