<div class="row p-t-20">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('division_code') ? 'has-error': '' }} ">
            {!! Form::label('division_code', 'Division Code', ['class'=>'form-label']) !!}
            {!! Form::text('division_code', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('division_code', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('division_name') ? 'has-error': '' }} ">
            {!! Form::label('division_name', 'Division Name', ['class'=>'form-label']) !!}
            {!! Form::text('division_name', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('division_name', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
</div>

{!! Form::submit ('Simpan',['class'=>'btn btn-primary m-b-10 m-l-5']) !!}