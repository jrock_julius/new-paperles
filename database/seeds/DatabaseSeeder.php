<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);

        $this->call(Master002DocumentSeeder::class);
        $this->call(Master003DocumentTypeSeeder::class);
        $this->call(Master004StatusTypeSeeder::class);
        $this->call(Master005StatusSeeder::class);
        $this->call(Master006DivisionSeeder::class);
        $this->call(Master007LocationSeeder::class);
        $this->call(Master008PlantSeeder::class);
        $this->call(Master009UomSeeder::class);
        $this->call(Master010MaterialTypeSeeder::class);
        $this->call(Master011ProcurementTypeSeeder::class);
        $this->call(Master012MaterialSeeder::class);
        $this->call(Master013MovementTypeSeeder::class);
        $this->call(Master014StorageLocationSeeder::class);
        $this->call(Master015PositionSeeder::class);
        $this->call(Master016ProjectSeeder::class);
        $this->call(Master018ToolSeeder::class);
        $this->call(Master032WorkShopSeeder::class);
        $this->call(Master017WorkCenterSeeder::class);
        $this->call(Master028SkillSeeder::class);
        $this->call(Master030ProcessSeeder::class);
        $this->call(Master031ProcessSkillSeeder::class);
        $this->call(Master001UserSeeder::class);
        $this->call(Master033VendorSeeder::class);
        $this->call(Master029UserSkillSeeder::class);
        $this->call(MasterRouting1ProductStructureSeeder::class);
        $this->call(MasterRouting2ProcessStructureSeeder::class);
        $this->call(MasterRouting3ProcessStructureDtlSeeder::class);
        $this->call(MasterRouting4MaterialProcessSeeder::class);
        $this->call(MasterRouting5ConsumableWorkCenterSeeder::class);
        $this->call(MasterRouting6ToolWorkCenterSeeder::class);

    }
}
