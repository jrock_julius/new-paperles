<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\mUserDataTable;
use App\mPlant;
use App\mDivision;
use App\mPosition;
use App\Role;
use App\User;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class mUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(mUserDataTable $dataTable)
    {
        return $dataTable->render('master.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plant = mPlant::get();
        $div = mDivision::get();
        $position = mPosition::get();
        $list_role = Role::get();
        
        return view('master.user.create', compact('plant','div','position','list_role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $folder = 'images/karyawan';
        $uploaded = 0;

        // $role = Role::where('id',$request->role_id);

        $file = $request->image_pf;

        $filename = $file->getClientOriginalName();
        $ext = strtolower($file->getClientOriginalExtension());

        

        $document = md5($filename. $request->nip. Carbon::today()->toDateTimeString()).'.'. $ext;

        $upload_path = $file->storeAs($folder, $document);
       

        $this->validate($request , [
            'nip'       => 'required|unique:users,nip',
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|confirmed|min:6',
            'role_id'   =>  'required'
        ]);
        
        $user = new User();
        $user->nip = $request->nip;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->plant_id = $request->plant_id;
        $user->division_id = $request->division_id;
        $user->position_id = $request->position_id;
        $user->path = $upload_path;
        $user->save();

        $role = Role::find($request->role_id);
        foreach($role as $ur)
        {
            $user->attachRole($ur);
        }

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $user->nip"
        ]);

        return redirect()->route('user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $users = User::with('roles')->find($id);
        $plant = mPlant::get();
        $division = mDivision::get();
        $position = mPosition::get();
        $user_roles = Role::whereIn('id',$users->roles->pluck('id'))->get();

        if ($user->hasRole('superadmin')){
            $roles = Role::get();
            $user = User::find($id);
            return view('master.user.edit', compact('roles','user','user_roles','plant','division','position'));
        }elseif ($user->hasRole('admin')){
            $roles = Role::where('id', '>=', '2')->get();
            $user = User::find($id);
            return view('master.user.edit', compact('roles','user','user_roles','plant','division','position'));
        }
        return view('master.user.index');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request , [
            'nip'       => 'required|unique:users,nip,'.$id,
            'role_id'   => 'required',
            'name'      => 'required',
            'email'     => 'required',
            'password'  => 'required|confirmed|min:6',
        ]);

        $newUserRole = Role::find($request->role_id);
        $user = User::find($id);
        $user->nip  = $request->nip;
        $user->name  = $request->name;
        $user->email  = $request->email;
        $user->password = bcrypt($request->password);
        $user->plant_id = $request->plant_id;
        $user->position_id = $request->position_id;
        $user->division_id = $request->division_id;
        
        $currentUserRole = Role::whereIn('id',$user->roles->pluck('id'))->get();
        
        foreach($currentUserRole as $cur){
            $user->detachRole($cur);
        }

        if(!$user->hasRole($newUserRole))
            foreach($newUserRole as $ur)
                $user->attachRole($ur);

        $user->save();

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $user->nip"
        ]);

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);

        User::destroy($id);

        Session::flash("flash_notification",[
            "level" => "danger",
            "message" => "Berhasil menghapus $user->nip"
        ]);

        return redirect()->route("user.index");
    }
}
