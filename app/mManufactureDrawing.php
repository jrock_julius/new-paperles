<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class mManufactureDrawing extends Model implements AuditableContract
{
    //
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'file_master_id',
        'file_name',
        'title',
        'path',
        'no_doc_is',
        'destination_shop',
        'revision',
        'state',
        'version',
        'material',
        'master_part',
        'part_number',
        'document_type',
        'project',
        'qty',
        'description',
        'shop_1',
        'shop_2',
        'shop_3',
        'ts_1',
        'ts_2',
        'ts_3',
        'ts_4',
        'ts_5',
        'ts_6',
        'ts_7',
        'ts_8',
        'ws_2',
        'ws_3',
        'ws_4',
        'ws_5',
        'ws_6',
        'ws_7',
        'ws_8',
        'prepared_by'
    ];

}
