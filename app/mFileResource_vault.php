<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mFileResource_vault extends Model
{
    //
    protected $connection = 'sqlsrv';
    protected $table = 'FileResource';
    protected $primarykey = 'ResourceId';
    protected $fillable =['ResourceId',
                         'FileMasterId',
                         'Version',
                         'Extension',
];
}
