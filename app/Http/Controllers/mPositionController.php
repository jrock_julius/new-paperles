<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\mPositionDataTable;
use App\mPosition;
use Session;


class mPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(mPositionDataTable $dataTable)
    {
        //
        return $dataTable->render('master.position.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request , [
            'position_code'    => 'required|unique:m_positions,position_code',
            'position_name'    => 'required'
        ]);
        // dd(mPosition::get());
        $position = mPosition::create([
            'position_code'    => $request->position_code,
            'position_name'    => $request->position_name
        ]);

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $position->position_name"
        ]);

        return redirect()->route('position.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = mPosition::find($id);
        return view('master.position.edit', compact('position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $this->validate($request , [
            'position_code'    => 'required|unique:m_positions,position_code,'.$id,
            'position_name'    => 'required'
        ]);

        $position = mPosition::find($id);
        $position->position_code  = $request->position_code;
        $position->position_name  = $request->position_name;
        $position->save();

        // dd($position);
        
        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $position->position_name"
        ]);

        return redirect()->route('position.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $position = mPosition::find($id);

        mPosition::destroy($id);

        Session::flash("flash_notification",[
            "level" => "success",
            "message" => "Berhasil menghapus $position->position_name"
        ]);

        return redirect()->route("position.index");
    }
}
