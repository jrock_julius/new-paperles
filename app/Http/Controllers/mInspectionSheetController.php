<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\mProjectISDataTable;
use App\DataTables\mInspectionSheetWIPDataTable;
use App\DataTables\mInspectionSheetRelDataTable;

class mInspectionSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(mProjectISDataTable $dataTable)
    {
        //
        return $dataTable->render('master.is.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function work_in_progress(mInspectionSheetWIPDataTable $dataTable)
    {
        //
        return $dataTable->render('master.is.index');
    }

    public function released(mInspectionSheetRelDataTable $dataTable)
    {
        //
        return $dataTable->render('master.is.index');
    }
}
