<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mFileMaster_vault extends Model
{
    //
    protected $connection = 'sqlsrv';
    protected $table = 'FileMaster';
    protected $primarykey = 'FileMastedID';
    protected $fillable =['FileMasterID',
                         'CheckedOut',
                         'CheckoutLocalSpec',
                         'CheckoutMachine',
                         'FileClassification',
                         'Hidden',
                         'FileStatus',
                         'Unmanaged',
                         'TipFileBaseName',
];
}
