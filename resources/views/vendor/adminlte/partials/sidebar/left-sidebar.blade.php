<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    @if(config('adminlte.logo_img_xl'))
        @include('adminlte::partials.common.brand-logo-xl')
    @else
        @include('adminlte::partials.common.brand-logo-xs')
    @endif

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <!-- <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('adminlte.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if(config('adminlte.sidebar_nav_animation_speed') != 300)
                    data-animation-speed="{{ config('adminlte.sidebar_nav_animation_speed') }}"
                @endif
                @if(!config('adminlte.sidebar_nav_accordion'))
                    data-accordion="true"
                @endif>
                {{-- Configured sidebar links --}}
                @each('adminlte::partials.sidebar.menu-item', $adminlte->menu('sidebar'), 'item') 

                <li class="nav-header">MASTER</li>

                              
            </ul>
        </nav> -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
            <li class="nav-item">
              <a href="{{ route('home') }}" class="{{ (request()->segment(1) == 'home') ? 'nav-link active' : 'nav-link inactive' }}">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  HOME
                </p>
              </a>
            </li>
            @role('superadmin')
            <li class="nav-item has-treeview menu-open">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-database"></i>
                <p>
                  MASTER
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('user.index') }}" class="{{ (request()->segment(1) == 'user') ? 'nav-link active' : 'nav-link inactive' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>User</p>
                  </a>
                </li>
                <li class="nav-item">
                <a href="{{ route('division.index') }}" class="{{ (request()->segment(1) == 'division') ? 'nav-link active' : 'nav-link inactive' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Division</p>
                  </a>
                </li>
                <li class="nav-item">
                <a href="{{ route('position.index') }}" class="{{ (request()->segment(1) == 'position') ? 'nav-link active' : 'nav-link inactive' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Position</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview menu-open">
              <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar"></i>
                <p>
                  Documents
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('md.index') }}" class="{{ (request()->segment(1) == 'md') ? 'nav-link active' : 'nav-link inactive' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manufacture Drawing</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('is.index') }}" class="{{ (request()->segment(1) == 'is') ? 'nav-link active' : 'nav-link inactive' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Inspection Sheet</p>
                  </a>
                </li>
              </ul>
            </li>
            @endrole
          </ul>
        </nav>
    </div>

</aside>
