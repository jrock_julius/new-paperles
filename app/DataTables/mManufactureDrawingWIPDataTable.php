<?php

namespace App\DataTables;

use App\mManufactureDrawing;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class mManufactureDrawingWIPDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()

            ->addColumn('edit', function($request){
                return view ('datatable._edit',[
                    'model'    => $request  ,
                    'edit_url' => route('user.edit', $request->id)
                ]);                       
            })

            ->addColumn('delete', function($request){    
                return view ('datatable._delete',[
                    'model'    => $request,
                    'delete_url' => route('user.destroy', $request->id),
                    'confirm_message' => 'Yakin mau menghapus?' . $request->file_name . '?'
                ]);                 
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\mManufactureDrawingWIP $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(mManufactureDrawing $model)
    {
        return $model->newQuery()->where('state','Work In Progress');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('mmanufacturedrawingwip-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('edit')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::computed('delete')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::make('DT_RowIndex')->orderable(false)->title('No'),
            Column::make('file_name'),
            Column::make('path'),
            Column::make('no_doc_is'),
            Column::make('destination_shop'),
            Column::make('revision'),
            Column::make('state'),
            Column::make('material'),
            Column::make('master_part'),
            Column::make('part_number'),
            Column::make('project'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mManufactureDrawingWIP_' . date('YmdHis');
    }
}
