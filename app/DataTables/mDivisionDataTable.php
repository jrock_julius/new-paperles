<?php

namespace App\DataTables;

use App\mDivision;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class mDivisionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()

        ->addColumn('edit', function($request){
            return view ('datatable._edit',[
                'model'    => $request  ,
                'edit_url' => route('division.edit', $request->id)
            ]);                       
        })

        ->addColumn('delete', function($request){    
            return view ('datatable._delete',[
                'model'    => $request,
                'delete_url' => route('division.destroy', $request->id),
                'confirm_message' => 'Yakin mau menghapus?' . $request->file_name . '?'
            ]);                 
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\mDivision $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(mDivision $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('mdivision-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('edit')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::computed('delete')
                ->exportable(false)
                ->printable(false)->orderable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::make('DT_RowIndex')->orderable(false)->title('No'),
            Column::make('division_code'),
            Column::make('division_name'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mDivision_' . date('YmdHis');
    }
}
