<div class="row p-t-20">
<div class="col-md-2">
        <div class="form-group{{ $errors->has('nip') ? 'has-error': '' }} ">
            {!! Form::label('nip', 'NIP', ['class'=>'form-label']) !!}
            {!! Form::text('nip', null, ['class'=>'form-control', 'maxlength' => '9']) !!} 
            {!! $errors->first('nip', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('name') ? 'has-error': '' }} ">
            {!! Form::label('name', 'Nama', ['class'=>'form-label']) !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('name', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('division_id') ? 'has-error': '' }} ">
            {!! Form::label('division_id', 'Division', ['class'=>'form-label']) !!}
            {!! Form::select('division_id', $div->pluck('division_name','id')->all(), null, [ 'class' => 'js-selectize form-control']) !!}
            {!! $errors->first('division_id', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
</div>
    <div class="row p-t-20">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('position_id') ? 'has-error': '' }} ">
            {!! Form::label('position_id', 'Position', ['class'=>'form-label']) !!}
            {!! Form::select('position_id', $position->pluck('position_name','id')->all(), null, [ 'class' => 'form-control js-selectize','placeholder' => 'Pilih Position', 'id' => 'position_id']) !!}
            {!! $errors->first('position_id', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('email') ? 'has-error': '' }} ">
            {!! Form::label('email', 'Email', ['class'=>'form-label']) !!}
            {!! Form::text('email', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('email', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('password') ? 'has-error': '' }} ">
            {!! Form::label('password', 'Password', ['class'=>'form-label']) !!}
            {!! Form::password('password', ['class'=>'form-control', 'id' => 'password']) !!} 
            {!! $errors->first('password', '<small class="text-danger">:message</small>') !!}
        </div>   
    </div>

    <div class="col-md-2">
        <div class="form-group{{ $errors->has('password_confirmation') ? 'has-error': '' }} ">
            {!! Form::label('password_confirmation', 'Confirm Password', ['class'=>'form-label']) !!}
            {!! Form::password('password_confirmation', ['class'=>'form-control']) !!} 
            {!! $errors->first('password_confirmation', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group {{ $errors->has('role_id') ? 'has-error': '' }} ">
            {!! Form::label('role_id', 'Role User', ['class'=>'form-label']) !!}
            {!! Form::select('role_id[]', $list_role->pluck('display_name','id')->all(), null, [ 'class' => 'form-control multi-selectize', 'multiple'=>'multiple', 'placeholder' => 'Pilih Role', 'id' => 'role_id']) !!}
            {!! $errors->first('role_id', '<small class="text-danger">:message</small>') !!}
        </div>   
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('id_card') ? 'has-error': '' }} ">
            {!! Form::label('id_card', 'ID Card', ['class'=>'form-label']) !!}
            {!! Form::text('id_card', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('id_card', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group form-float{{ $errors->has('image_pf') ? 'has-error' : '' }}">
        {!! Form::label('image_pf','Foto Karyawan', ['class'=> 'form-label']) !!}
        {!! Form::file('image_pf'); !!}
        {!! $errors->first('image_pf', '<p class="error">:message</p>') !!}
        </div>
    </div>
</div>

{!! Form::submit ('Simpan',['class'=>'btn btn-primary m-b-10 m-l-5']) !!}