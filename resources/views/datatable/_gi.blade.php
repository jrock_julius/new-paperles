{!! Form::model($model, ['url' => $gi_url, 'method' => 'get', 'class' => 'form-inline'] ) !!}
{!! Form::button('<i class="fa fa-people-carry" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-success', 'data-toggle' => 'tooltip', 'title' => 'GI')) !!}
{!! Form::close() !!}