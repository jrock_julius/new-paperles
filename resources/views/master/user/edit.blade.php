@extends('adminlte::page')

@section('title', 'User - Manufacturing 4.0')

@section('content_header')
    <h1>User</h1>
@stop

@section('content')
<div class="right_col" role="main">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
        <h4 class="c-grey-900 mT-10 mB-30">Management User</h4> </div>
        <div class="col-md-6 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Master Data</li>
                <li class="breadcrumb-item">Document</li>
                <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::model($user, ['url' => route('user.update', $user->id),
                'method' => 'put' , 'id' => 'form_validation']) !!}
                @include('master.user._form_edit')
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- validator -->
<script src="{{ asset('gentelella/vendors/validator/validator.js') }}"></script>
@endsection

@section('js')
    <script>
        $('.multi-selectize').selectize({
            plugins : ['remove_button'],
            maxItems : null,
            delimiter : ',',
            persist : true,
        });
    </script>
@endsection