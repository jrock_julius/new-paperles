<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tMrp;
use App\tProduct;
use App\tProductionOrder;
use App\tProdOrdProcess;
use App\tPoProcessOperator;
use App\mStatus;
use App\mStatusType;
use App\User;
use Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('monitoring'))
        {
            $mrp = tMrp::with('product_structure.material.uom', 'project')->get();

            $list_mrp = collect();
            if(count($mrp) <= 0)
            {
                $list_mrp = $list_mrp->push(['id' => '' , 'label' => '']);
            }
            else
            {
                foreach($mrp as $p)
                {
                    $id                 = $p->id;
                    
                    $code               = $p->mrp_code;
                    $project            = $p->project->project_description;
                    $qty                = $p->project->qty;
                    $uom                = $p->product_structure->material->uom->uom_code;
                    $material           = $p->product_structure->material->material_description;
                    $serial             = $p->product_structure->serial_code;
                    $label              = "(".$code.") ".$project.' '.$qty.' '.$uom.' '.$material.' '.$serial;
                    
                    $list_mrp     = $list_mrp->push(['id' => $id ,'label' => $label]);
                }
            }

            $t_mrp = tMrp::get();
            $projects = $t_mrp->count();

            $status_type = mStatusType::where('status_type_code','PO')->first();
            $status_finpo = mStatus::where('status_type_id',$status_type->id)->where('status_code','FIN')->first();
            $t_product = tProduct::get();
            $t_prod_order = tProductionOrder::get();
            $total_product = $t_product->count();
            $finish_product = 0;
            foreach($t_product as $p)
            {
                $total = $t_prod_order->where('product_id', $p->id)->count();
                $finish = $t_prod_order->where('product_id', $p->id)->where('status_id', $status_finpo->id)->count();

                if($finish == $total)
                {
                    $finish_product++;
                }
            }
            $products = $finish_product."/".$total_product;

            $status_type = mStatusType::where('status_type_code','POP')->first();
            $status_pop = mStatus::where('status_type_id',$status_type->id)->where('status_code','ON')->first();
            $prod_ord_on_process = tProdOrdProcess::where('status_id',$status_pop->id)->get();
            $prod_proc_user = tPoProcessOperator::whereIn('prod_ord_process_id',$prod_ord_on_process->pluck('id'))->get(); 
            $available_user = User::whereHas(
                'roles', function($q){
                    $q->where('name', 'operator');
                }
            )->whereNotIn('id',$prod_proc_user->pluck('user_id'))->get();
            $onduty_user = User::whereHas(
                'roles', function($q){
                    $q->where('name', 'operator');
                }
            )->whereIn('id',$prod_proc_user->pluck('user_id'))->get();

            $onduty = $onduty_user->count();
            $available = $available_user->count();

            return view('dashboard', compact('list_mrp','projects','products','onduty','available'));
        }
        else
        {
            return view('home');
        }
    }
}
